%{
#include "gexpr.h"
#include <stdio.h>
%}

%lex-param { ge_expr_prglist_t *ctx }
%parse-param { ge_expr_prglist_t *ctx }

%union {
	double num;
	int idx;
}

%token <num> T_NUM
%token <idx> T_PARAM

%left '+' '-'
%left 'x' '/'
%left UMINUS

%%

expr:
	  '(' expr ')'
	| '-'                    { gex_append(ctx, PUSH_NUM, 0); }
	      expr %prec UMINUS    { gex_append(ctx, SUB, 0); }
	| expr '+' expr          { gex_append(ctx, ADD, 0); }
	| expr '-' expr          { gex_append(ctx, SUB, 0); }
	| expr 'x' expr          { gex_append(ctx, MUL, 0); }
	| expr '/' expr          { gex_append(ctx, DIV, 0); }
	| T_NUM                  { gex_append(ctx, PUSH_NUM, $1); }
	| T_PARAM                { gex_append_idx(ctx, PUSH_PARAM, $1); }
	;

%%
