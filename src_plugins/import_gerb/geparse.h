/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer - low level gerber parser
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include "gedraw.h"

#include <genht/htsp.h>

typedef struct geparse_ctx_s geparse_ctx_t;

struct geparse_ctx_s {
	/* public: parser state */
	long line, col, cmd_cnt;
	const char *errmsg;

	/* public: caller provided functions */
	int (*get_char)(geparse_ctx_t *ctx); /* read the next character from the stream */

	/* public: data used only by the caller */
	void *user_data;

	/* private: parser state */
	ge_unit_t unit;
	int cfmt_int, cfmt_fra; /* coordinate format: integer and fraction digits */
	int ungetc;
	unsigned trailing_zero:1; /* coords are padded with trailing zeros */
	unsigned at_end:1;

	/* private: draw program */
	gedraw_ctx_t draw;
	htsp_t *macros; /* both key and values pointing into the bytecode */
};


ge_parse_res_t geparse(geparse_ctx_t *ctx);
void geparse_free(geparse_ctx_t *ctx);

