#include <stdio.h>
#include <ctype.h>
#include "gexpr.h"

extern int gexparse(ge_expr_prglist_t *ctx);

int gexlex(YYSTYPE *lval, ge_expr_prglist_t *ctx_)
{
	char tmp[128], *end;
	int ti, c = getchar();
	switch(c) {
		case '(': case ')': case '+': case '-': case 'x': case '/': return c;
		case '$':
			c = getchar();
			for(ti = 0; isdigit(c) && (ti < sizeof(tmp)-1); c = getchar(),ti++)
				tmp[ti] = c;
			tmp[ti] = '\0';
			ungetc(c, stdin);
			lval->idx = atoi(tmp);
			return T_PARAM;
		case '0': case '1': case '2': case '3': case '4': case '5':
		case '6': case '7': case '8': case '9': case '.':
			for(ti = 0; (isdigit(c) || (c == '.')) && (ti < sizeof(tmp)-1); c = getchar(),ti++)
				tmp[ti] = c;
			tmp[ti] = '\0';
			ungetc(c, stdin);
			lval->num = strtod(tmp, &end);
			return T_NUM;
		case '\n': return 0;
	}
	return c;
}

int gexerror(ge_expr_prglist_t *ctx_, const char *msg)
{
	printf("ERROR: %s\n", msg);
	return 0;
}

int main()
{
	ge_expr_prglist_t lst;
	vtd0_t params;
	double res;

	memset(&lst, 0, sizeof(ge_expr_prglist_t));
	gexparse(&lst);

	vtd0_init(&params);
	printf("eval: %d\n", gex_eval(lst.first, &params, &res));
	printf("res: %f\n", res);
	return 0;
}

