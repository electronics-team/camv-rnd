/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"
#include <librnd/config.h>

#include <librnd/core/actions.h>
#include <librnd/hid/hid_dad.h>

#include "data.h"
#include "event.h"

#include "dlg_layer.h"

typedef struct{
	RND_DAD_DECL_NOINIT(dlg)
	int widx, wname, wcolor;
	int active; /* already open - allow only one instance */
} layer_ctx_t;

layer_ctx_t layer_ctx;

static void layer_close_cb(void *caller_data, rnd_hid_attr_ev_t ev)
{
	layer_ctx_t *ctx = caller_data;
	RND_DAD_FREE(ctx->dlg);
	memset(ctx, 0, sizeof(layer_ctx_t)); /* reset all states to the initial - includes ctx->active = 0; */
}

static void layer_camv2dlg(layer_ctx_t *ctx)
{
	int lidx = rnd_actionva(&camv.hidlib, "Layer", "getidx", NULL);
	rnd_hid_attr_val_t hv;
	camv_layer_t *ly;
	char tmp[32], *short_name;

	if ((lidx < 0) && (lidx >= camv.layers.used)) {
		hv.str = "<n/a>";
		rnd_gui->attr_dlg_set_value(ctx->dlg_hid_ctx, ctx->wname, &hv);
		rnd_gui->attr_dlg_widget_state(ctx->dlg_hid_ctx, ctx->wcolor, 0);
		return;
	}

	ly = camv.layers.array[lidx];
	rnd_gui->attr_dlg_widget_state(ctx->dlg_hid_ctx, ctx->wcolor, 1);

	hv.str = tmp;
	sprintf(tmp, "%d", lidx);
	rnd_gui->attr_dlg_set_value(ctx->dlg_hid_ctx, ctx->widx, &hv);


	short_name = ly->short_name;
	if (short_name == NULL) {
		short_name = strrchr(ly->name, '/');
		if (short_name == NULL)
			short_name = ly->name;
		else
			short_name++;
	}

	hv.str = short_name;
	rnd_gui->attr_dlg_set_value(ctx->dlg_hid_ctx, ctx->wname, &hv);

	hv.clr = ly->color;
	rnd_gui->attr_dlg_set_value(ctx->dlg_hid_ctx, ctx->wcolor, &hv);
}

static void layer_clr_change_cb(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	rnd_actionva(&camv.hidlib, "Layer", "setcolor", attr->val.clr.str, NULL);
}

static void layer_name_change_cb(void *hid_ctx, void *caller_data, rnd_hid_attribute_t *attr)
{
	layer_ctx_t *ctx = caller_data;
	rnd_hid_attribute_t *na = &ctx->dlg[ctx->wname];
	rnd_actionva(&camv.hidlib, "Layer", "rename", na->val.str, NULL);
}

static void camv_rnd_dlg_layer(void)
{
	rnd_hid_dad_buttons_t clbtn[] = {{"Close", 0}, {NULL, 0}};

	if (layer_ctx.active)
		return;

	RND_DAD_BEGIN_VBOX(layer_ctx.dlg);
		RND_DAD_COMPFLAG(layer_ctx.dlg, RND_HATF_EXPFILL);
		RND_DAD_BEGIN_TABLE(layer_ctx.dlg, 2);
			RND_DAD_COMPFLAG(layer_ctx.dlg, RND_HATF_EXPFILL);

			RND_DAD_LABEL(layer_ctx.dlg, "Index:");
			RND_DAD_LABEL(layer_ctx.dlg, "-");
				layer_ctx.widx = RND_DAD_CURRENT(layer_ctx.dlg);

			RND_DAD_LABEL(layer_ctx.dlg, "Name:");
			RND_DAD_BEGIN_HBOX(layer_ctx.dlg);
				RND_DAD_STRING(layer_ctx.dlg);
					layer_ctx.wname = RND_DAD_CURRENT(layer_ctx.dlg);
					RND_DAD_ENTER_CB(layer_ctx.dlg, layer_name_change_cb);
				RND_DAD_BUTTON(layer_ctx.dlg, "set");
					RND_DAD_CHANGE_CB(layer_ctx.dlg, layer_name_change_cb);
			RND_DAD_END(layer_ctx.dlg);

			RND_DAD_LABEL(layer_ctx.dlg, "Color:");
			RND_DAD_COLOR(layer_ctx.dlg);
				layer_ctx.wcolor = RND_DAD_CURRENT(layer_ctx.dlg);
				RND_DAD_CHANGE_CB(layer_ctx.dlg, layer_clr_change_cb);

		RND_DAD_END(layer_ctx.dlg);
		RND_DAD_BUTTON_CLOSES(layer_ctx.dlg, clbtn);
	RND_DAD_END(layer_ctx.dlg);

	/* set up the context */
	layer_ctx.active = 1;

	RND_DAD_NEW("layer", layer_ctx.dlg, "Layer properties", &layer_ctx, rnd_false, layer_close_cb);

	layer_camv2dlg(&layer_ctx);
}

void camv_layer_dlg_layer_chg_ev(rnd_design_t *hidlib, void *user_data, int argc, rnd_event_arg_t argv[])
{
	if (layer_ctx.active)
		layer_camv2dlg(&layer_ctx);
}


const char camv_acts_LayerDialog[] = "LayerDialog()\n";
const char camv_acth_LayerDialog[] = "Present the LayerDialog box on the currently selected layer";
fgw_error_t camv_act_LayerDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	camv_rnd_dlg_layer();
	RND_ACT_IRES(0);
	return 0;
}
