/* original parser id follows */
/* yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93" */
/* (use YYMAJOR/YYMINOR for ifdefs dependent on parser version) */

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20140715

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)
#define YYENOMEM       (-2)
#define YYEOF          0

#ifndef yyparse
#define yyparse    gcodeparse
#endif /* yyparse */

#ifndef yylex
#define yylex      gcodelex
#endif /* yylex */

#ifndef yyerror
#define yyerror    gcodeerror
#endif /* yyerror */

#ifndef yychar
#define yychar     gcodechar
#endif /* yychar */

#ifndef yyval
#define yyval      gcodeval
#endif /* yyval */

#ifndef yylval
#define yylval     gcodelval
#endif /* yylval */

#ifndef yydebug
#define yydebug    gcodedebug
#endif /* yydebug */

#ifndef yynerrs
#define yynerrs    gcodenerrs
#endif /* yynerrs */

#ifndef yyerrflag
#define yyerrflag  gcodeerrflag
#endif /* yyerrflag */

#ifndef yylhs
#define yylhs      gcodelhs
#endif /* yylhs */

#ifndef yylen
#define yylen      gcodelen
#endif /* yylen */

#ifndef yydefred
#define yydefred   gcodedefred
#endif /* yydefred */

#ifndef yydgoto
#define yydgoto    gcodedgoto
#endif /* yydgoto */

#ifndef yysindex
#define yysindex   gcodesindex
#endif /* yysindex */

#ifndef yyrindex
#define yyrindex   gcoderindex
#endif /* yyrindex */

#ifndef yygindex
#define yygindex   gcodegindex
#endif /* yygindex */

#ifndef yytable
#define yytable    gcodetable
#endif /* yytable */

#ifndef yycheck
#define yycheck    gcodecheck
#endif /* yycheck */

#ifndef yyname
#define yyname     gcodename
#endif /* yyname */

#ifndef yyrule
#define yyrule     gcoderule
#endif /* yyrule */
#define YYPREFIX "gcode"

#define YYPURE 1

#line 2 "gcode.y"
/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019,2024 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include <stdio.h>
#include "gcode_vm.h"
#include "gcode_lex.h"
#line 36 "gcode.y"
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union {
	double num;
	int chr;
} YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
#line 143 "gcode.tab.c"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
# ifdef YYPARSE_PARAM_TYPE
#  define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
# else
#  define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
# endif
#else
# define YYPARSE_DECL() yyparse(gcode_prg_t * ctx)
#endif

/* Parameters sent to lex. */
#ifdef YYLEX_PARAM
# ifdef YYLEX_PARAM_TYPE
#  define YYLEX_DECL() yylex(YYSTYPE *yylval, YYLEX_PARAM_TYPE YYLEX_PARAM)
# else
#  define YYLEX_DECL() yylex(YYSTYPE *yylval, void * YYLEX_PARAM)
# endif
# define YYLEX yylex(&yylval, YYLEX_PARAM)
#else
# define YYLEX_DECL() yylex(YYSTYPE *yylval, gcode_prg_t * ctx)
# define YYLEX yylex(&yylval, ctx)
#endif

/* Parameters sent to yyerror. */
#ifndef YYERROR_DECL
#define YYERROR_DECL() yyerror(gcode_prg_t * ctx, const char *s)
#endif
#ifndef YYERROR_CALL
#define YYERROR_CALL(msg) yyerror(ctx, msg)
#endif

extern int YYPARSE_DECL();

#define T_NUM 257
#define T_DEC 258
#define T_CHR 259
#define T_NL 260
#define T_LINENO 261
#define T_ACOS 262
#define T_ASIN 263
#define T_ATAN 264
#define T_ABS 265
#define T_COS 266
#define T_SIN 267
#define T_TAN 268
#define T_FIX 269
#define T_FUP 270
#define T_EXP 271
#define T_LN 272
#define T_ROUND 273
#define T_SQRT 274
#define T_MOD 275
#define UMINUS 276
#define UPLUS 277
#define T_OR 278
#define T_XOR 279
#define T_AND 280
#define YYERRCODE 256
typedef short YYINT;
static const YYINT gcodelhs[] = {                        -1,
    0,    0,    1,    3,    1,    4,    1,    2,    2,    5,
    5,    5,    5,    5,    7,    5,    6,    8,    6,    9,
    6,    6,    6,    6,    6,    6,    6,    6,    6,    6,
    6,    6,    6,    6,    6,    6,    6,    6,    6,    6,
    6,    6,    6,    6,    6,
};
static const YYINT gcodelen[] = {                         2,
    2,    0,    1,    0,    5,    0,    3,    1,    2,    4,
    3,    2,    2,    3,    0,    5,    3,    0,    3,    0,
    3,    3,    3,    3,    3,    3,    3,    3,    3,    2,
    4,    8,    4,    4,    4,    4,    4,    4,    4,    4,
    4,    4,    4,    1,    1,
};
static const YYINT gcodedefred[] = {                      0,
    3,    0,    0,    0,    0,    4,    1,    0,   15,    0,
    0,    0,   13,   12,    0,    0,    0,    0,    7,    9,
    0,   14,   44,   45,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   20,   18,    0,
    0,    0,    0,    0,    5,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
   30,    0,    0,    0,    0,    0,    0,    0,    0,    0,
   10,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   17,    0,    0,
    0,    0,    0,   26,   27,   28,    0,   33,   34,    0,
   31,   35,   36,   37,   38,   39,   40,   41,   42,   43,
    0,    0,    0,   32,
};
static const YYINT gcodedgoto[] = {                       3,
    4,   10,   12,    5,   11,   42,   18,   60,   59,
};
static const YYINT gcodesindex[] = {                   -213,
    0, -255,    0, -213,  -26,    0,    0,   10,    0, -245,
  -26,  -26,    0,    0, -252,   18,   18,   18,    0,    0,
 -238,    0,    0,    0,  -71,  -63,  -39,  -37,  -34,  -32,
   -6,   -4,   -2,    4,    6,   12,   16,    0,    0,   18,
   18,  131,   23,   29,    0,   18,   18,   18,   18,   18,
   18,   18,   18,   18,   18,   18,   18,   18,   18,   18,
    0,   35,   18,   18,   18,   18,   18,   18,   18,   18,
    0,   18,   41,   51,   57,   63,   70,   76,   82,   88,
   94,  100,  106,  112,  119, -240, -240,    0,  137,  137,
 -240, -240, -240,    0,    0,    0,  131,    0,    0,    9,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
   20,   18,  125,    0,
};
static const YYINT gcoderindex[] = {                      1,
    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,
 -198,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  -33,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,  -35,  -29,    0,  -24,  159,
  -18,  -12,   -1,    0,    0,    0,  -31,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,
};
static const YYINT gcodegindex[] = {                     69,
    0,   39,    0,    0,    0,  384,    0,    0,    0,
};
#define YYTABLESIZE 496
static const YYINT gcodetable[] = {                      21,
    2,   11,    6,   16,   22,   19,   21,   21,    9,   21,
   22,   21,   19,   19,   19,   19,   24,   19,   22,   46,
   22,   45,   25,   24,   24,   21,   24,   47,   24,   25,
   25,   19,   25,   29,   25,    6,   22,   68,   69,   70,
   29,   29,   24,   29,   16,   29,    1,    2,   25,   20,
   21,   48,   40,   49,   15,  111,   50,   21,   51,   29,
   38,    8,   39,   19,   65,   63,    0,   64,   22,   66,
   65,   63,    7,   64,   24,   66,   65,   63,    0,   64,
   25,   66,   65,   63,   52,   64,   53,   66,   54,   72,
    0,   29,   65,   63,   55,   64,   56,   66,   65,   63,
   17,   64,   57,   66,   65,   63,   58,   64,   41,   66,
  112,   65,   63,    0,   64,   71,   66,   65,   63,    0,
   64,    0,   66,   65,   63,    0,   64,   88,   66,   65,
   63,    0,   64,   98,   66,   65,   63,    0,   64,    0,
   66,   65,   63,   99,   64,    0,   66,   65,   63,  100,
   64,    0,   66,   65,   63,  101,   64,    0,   66,    0,
   65,   63,  102,   64,    0,   66,   65,   63,  103,   64,
    0,   66,   65,   63,  104,   64,    0,   66,   65,    0,
  105,    0,    0,   66,    0,    0,  106,    0,    0,    0,
    0,    0,  107,   23,    0,    0,    0,    0,  108,    0,
    0,   23,    0,   23,  109,    0,    0,    0,    0,    0,
    0,  110,    0,    0,    0,    0,    0,  114,    0,   23,
    0,    0,    0,   21,   21,   11,   11,   16,   16,   19,
   19,    0,    8,    0,   22,   22,    0,    0,    0,   21,
   24,   24,    0,    0,    0,   19,   25,   25,    0,    0,
    0,   23,    0,    0,    0,    0,   24,   29,   29,    6,
    0,    0,   25,    0,    0,    0,   13,   14,    0,    0,
    0,    0,    0,   29,   23,   24,    0,    0,    0,   25,
   26,   27,   28,   29,   30,   31,   32,   33,   34,   35,
   36,   37,    0,    0,    0,    0,    0,   67,    0,    0,
   68,   69,   70,   67,    0,    0,   68,   69,   70,   67,
    0,    0,   68,   69,   70,   67,    0,    0,   68,   69,
   70,    0,    0,    0,    0,   67,    0,    0,   68,   69,
   70,   67,    0,    0,   68,   69,   70,   67,    0,    0,
   68,   69,   70,    0,   67,    0,    0,   68,   69,   70,
   67,    0,    0,   68,   69,   70,   67,    0,    0,   68,
   69,   70,   67,    0,    0,   68,   69,   70,   67,    0,
    0,   68,   69,   70,   67,    0,    0,   68,   69,   70,
   67,    0,    0,   68,   69,   70,   67,    0,    0,   68,
   69,   70,    0,   67,    0,    0,   68,   69,   70,   67,
   43,   44,   68,   69,   70,   67,    0,    0,   68,   69,
   70,   67,    0,    0,   68,   69,   70,   23,   23,    0,
    0,    0,    0,   61,   62,    0,    0,    0,    0,   73,
   74,   75,   76,   77,   78,   79,   80,   81,   82,   83,
   84,   85,   86,   87,    0,    0,   89,   90,   91,   92,
   93,   94,   95,   96,    0,   97,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,  113,
};
static const YYINT gcodecheck[] = {                      35,
    0,   35,  258,   35,  257,   35,   42,   43,   35,   45,
   35,   47,   42,   43,  260,   45,   35,   47,   43,   91,
   45,  260,   35,   42,   43,   61,   45,   91,   47,   42,
   43,   61,   45,   35,   47,   35,   61,  278,  279,  280,
   42,   43,   61,   45,   35,   47,  260,  261,   61,   11,
   12,   91,   35,   91,   45,   47,   91,   93,   91,   61,
   43,  260,   45,   93,   42,   43,   -1,   45,   93,   47,
   42,   43,    4,   45,   93,   47,   42,   43,   -1,   45,
   93,   47,   42,   43,   91,   45,   91,   47,   91,   61,
   -1,   93,   42,   43,   91,   45,   91,   47,   42,   43,
   91,   45,   91,   47,   42,   43,   91,   45,   91,   47,
   91,   42,   43,   -1,   45,   93,   47,   42,   43,   -1,
   45,   -1,   47,   42,   43,   -1,   45,   93,   47,   42,
   43,   -1,   45,   93,   47,   42,   43,   -1,   45,   -1,
   47,   42,   43,   93,   45,   -1,   47,   42,   43,   93,
   45,   -1,   47,   42,   43,   93,   45,   -1,   47,   -1,
   42,   43,   93,   45,   -1,   47,   42,   43,   93,   45,
   -1,   47,   42,   43,   93,   45,   -1,   47,   42,   -1,
   93,   -1,   -1,   47,   -1,   -1,   93,   -1,   -1,   -1,
   -1,   -1,   93,   35,   -1,   -1,   -1,   -1,   93,   -1,
   -1,   43,   -1,   45,   93,   -1,   -1,   -1,   -1,   -1,
   -1,   93,   -1,   -1,   -1,   -1,   -1,   93,   -1,   61,
   -1,   -1,   -1,  259,  260,  259,  260,  259,  260,  259,
  260,   -1,  259,   -1,  259,  260,   -1,   -1,   -1,  275,
  259,  260,   -1,   -1,   -1,  275,  259,  260,   -1,   -1,
   -1,   93,   -1,   -1,   -1,   -1,  275,  259,  260,  259,
   -1,   -1,  275,   -1,   -1,   -1,  257,  258,   -1,   -1,
   -1,   -1,   -1,  275,  257,  258,   -1,   -1,   -1,  262,
  263,  264,  265,  266,  267,  268,  269,  270,  271,  272,
  273,  274,   -1,   -1,   -1,   -1,   -1,  275,   -1,   -1,
  278,  279,  280,  275,   -1,   -1,  278,  279,  280,  275,
   -1,   -1,  278,  279,  280,  275,   -1,   -1,  278,  279,
  280,   -1,   -1,   -1,   -1,  275,   -1,   -1,  278,  279,
  280,  275,   -1,   -1,  278,  279,  280,  275,   -1,   -1,
  278,  279,  280,   -1,  275,   -1,   -1,  278,  279,  280,
  275,   -1,   -1,  278,  279,  280,  275,   -1,   -1,  278,
  279,  280,  275,   -1,   -1,  278,  279,  280,  275,   -1,
   -1,  278,  279,  280,  275,   -1,   -1,  278,  279,  280,
  275,   -1,   -1,  278,  279,  280,  275,   -1,   -1,  278,
  279,  280,   -1,  275,   -1,   -1,  278,  279,  280,  275,
   17,   18,  278,  279,  280,  275,   -1,   -1,  278,  279,
  280,  275,   -1,   -1,  278,  279,  280,  259,  260,   -1,
   -1,   -1,   -1,   40,   41,   -1,   -1,   -1,   -1,   46,
   47,   48,   49,   50,   51,   52,   53,   54,   55,   56,
   57,   58,   59,   60,   -1,   -1,   63,   64,   65,   66,
   67,   68,   69,   70,   -1,   72,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  112,
};
#define YYFINAL 3
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 280
#define YYUNDFTOKEN 292
#define YYTRANSLATE(a) ((a) > YYMAXTOKEN ? YYUNDFTOKEN : (a))
#if YYDEBUG
static const char *const gcodename[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,"'#'",0,0,0,0,0,0,"'*'","'+'",0,"'-'",0,"'/'",0,0,0,0,0,0,0,0,0,0,0,0,0,"'='",
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"'['",0,"']'",0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
"T_NUM","T_DEC","T_CHR","T_NL","T_LINENO","T_ACOS","T_ASIN","T_ATAN","T_ABS",
"T_COS","T_SIN","T_TAN","T_FIX","T_FUP","T_EXP","T_LN","T_ROUND","T_SQRT",
"T_MOD","UMINUS","UPLUS","T_OR","T_XOR","T_AND",0,0,0,0,0,0,0,0,0,0,0,
"illegal-symbol",
};
static const char *const gcoderule[] = {
"$accept : program",
"program : line program",
"program :",
"line : T_NL",
"$$1 :",
"line : T_LINENO T_DEC $$1 codes T_NL",
"$$2 :",
"line : $$2 codes T_NL",
"codes : gcode",
"codes : gcode codes",
"gcode : T_CHR '[' expr ']'",
"gcode : T_CHR '#' expr",
"gcode : T_CHR T_DEC",
"gcode : T_CHR T_NUM",
"gcode : T_CHR '-' T_NUM",
"$$3 :",
"gcode : '#' $$3 expr '=' expr",
"expr : '[' expr ']'",
"$$4 :",
"expr : '-' $$4 expr",
"$$5 :",
"expr : '+' $$5 expr",
"expr : expr '+' expr",
"expr : expr '-' expr",
"expr : expr '*' expr",
"expr : expr '/' expr",
"expr : expr T_OR expr",
"expr : expr T_XOR expr",
"expr : expr T_AND expr",
"expr : expr T_MOD expr",
"expr : '#' expr",
"expr : T_ABS '[' expr ']'",
"expr : T_ATAN '[' expr ']' '/' '[' expr ']'",
"expr : T_ACOS '[' expr ']'",
"expr : T_ASIN '[' expr ']'",
"expr : T_COS '[' expr ']'",
"expr : T_SIN '[' expr ']'",
"expr : T_TAN '[' expr ']'",
"expr : T_FIX '[' expr ']'",
"expr : T_FUP '[' expr ']'",
"expr : T_EXP '[' expr ']'",
"expr : T_LN '[' expr ']'",
"expr : T_ROUND '[' expr ']'",
"expr : T_SQRT '[' expr ']'",
"expr : T_NUM",
"expr : T_DEC",

};
#endif

int      yydebug;
int      yynerrs;

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 10000
#define YYMAXDEPTH  10000
#endif
#endif

#define YYINITSTACKSIZE 200

typedef struct {
    unsigned stacksize;
    YYINT    *s_base;
    YYINT    *s_mark;
    YYINT    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;

#if YYDEBUG
#include <stdio.h>		/* needed for printf */
#endif

#include <stdlib.h>	/* needed for malloc, etc */
#include <string.h>	/* needed for memset */

/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    YYINT *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return YYENOMEM;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = (int) (data->s_mark - data->s_base);
    newss = (YYINT *)realloc(data->s_base, newsize * sizeof(*newss));
    if (newss == 0)
        return YYENOMEM;

    data->s_base = newss;
    data->s_mark = newss + i;

    newvs = (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs));
    if (newvs == 0)
        return YYENOMEM;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int      yyerrflag;
    int      yychar;
    YYSTYPE  yyval;
    YYSTYPE  yylval;

    /* variables for the parser stack */
    YYSTACKDATA yystack;
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack) == YYENOMEM) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = YYLEX) < 0) yychar = YYEOF;
#if YYDEBUG
        if (yydebug)
        {
            yys = yyname[YYTRANSLATE(yychar)];
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    YYERROR_CALL("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == YYEOF) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = yyname[YYTRANSLATE(yychar)];
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 4:
#line 63 "gcode.y"
	{ gcode_set_lineno(ctx, (int)yystack.l_mark[0].num); gcode_append(ctx, T_LINENO, yystack.l_mark[0].num); }
break;
case 5:
#line 64 "gcode.y"
	{ gcode_delayed(ctx, GCP_DELAY_APPLY); gcode_delayed(ctx, GCP_DELAY_OFF); gcode_append(ctx, DO, -1); }
break;
case 6:
#line 65 "gcode.y"
	{ gcode_append(ctx, T_LINENO, -1); ctx->lineno = -1; }
break;
case 7:
#line 66 "gcode.y"
	{ gcode_delayed(ctx, GCP_DELAY_APPLY); gcode_delayed(ctx, GCP_DELAY_OFF); gcode_append(ctx, DO, -1); }
break;
case 10:
#line 75 "gcode.y"
	{ gcode_append(ctx, yystack.l_mark[-3].chr, -1); }
break;
case 11:
#line 76 "gcode.y"
	{ gcode_append(ctx, PARAM, 0); gcode_append(ctx, yystack.l_mark[-2].chr, -1)->do_pop = 1; }
break;
case 12:
#line 77 "gcode.y"
	{ gcode_append(ctx, yystack.l_mark[-1].chr, yystack.l_mark[0].num); }
break;
case 13:
#line 78 "gcode.y"
	{ gcode_append(ctx, yystack.l_mark[-1].chr, yystack.l_mark[0].num); }
break;
case 14:
#line 79 "gcode.y"
	{ gcode_append(ctx, yystack.l_mark[-2].chr, -(yystack.l_mark[0].num)); }
break;
case 15:
#line 81 "gcode.y"
	{ gcode_delayed(ctx, GCP_DELAY_ON); }
break;
case 16:
#line 83 "gcode.y"
	{ gcode_append(ctx, ASSIGN, 0); gcode_delayed(ctx, GCP_DELAY_OFF); }
break;
case 18:
#line 88 "gcode.y"
	{  gcode_append(ctx, PUSH_NUM, 0); }
break;
case 19:
#line 89 "gcode.y"
	{ gcode_append(ctx, SUB, 0); }
break;
case 20:
#line 90 "gcode.y"
	{ /* nothing to do */ }
break;
case 22:
#line 92 "gcode.y"
	{ gcode_append(ctx, ADD, 0); }
break;
case 23:
#line 93 "gcode.y"
	{ gcode_append(ctx, SUB, 0); }
break;
case 24:
#line 94 "gcode.y"
	{ gcode_append(ctx, MUL, 0); }
break;
case 25:
#line 95 "gcode.y"
	{ gcode_append(ctx, DIV, 0); }
break;
case 26:
#line 96 "gcode.y"
	{ gcode_append(ctx, T_OR, 0); }
break;
case 27:
#line 97 "gcode.y"
	{ gcode_append(ctx, T_XOR, 0); }
break;
case 28:
#line 98 "gcode.y"
	{ gcode_append(ctx, T_AND, 0); }
break;
case 29:
#line 99 "gcode.y"
	{ gcode_append(ctx, T_MOD, 0); }
break;
case 30:
#line 100 "gcode.y"
	{ gcode_append(ctx, PARAM, 0); }
break;
case 31:
#line 101 "gcode.y"
	{ gcode_append(ctx, T_ABS, 0); }
break;
case 32:
#line 103 "gcode.y"
	{ gcode_append(ctx, T_ATAN, 0); }
break;
case 33:
#line 104 "gcode.y"
	{ gcode_append(ctx, T_ACOS, 0); }
break;
case 34:
#line 105 "gcode.y"
	{ gcode_append(ctx, T_ASIN, 0); }
break;
case 35:
#line 106 "gcode.y"
	{ gcode_append(ctx, T_COS, 0); }
break;
case 36:
#line 107 "gcode.y"
	{ gcode_append(ctx, T_SIN, 0); }
break;
case 37:
#line 108 "gcode.y"
	{ gcode_append(ctx, T_TAN, 0); }
break;
case 38:
#line 109 "gcode.y"
	{ gcode_append(ctx, T_FIX, 0); }
break;
case 39:
#line 110 "gcode.y"
	{ gcode_append(ctx, T_FUP, 0); }
break;
case 40:
#line 111 "gcode.y"
	{ gcode_append(ctx, T_EXP, 0); }
break;
case 41:
#line 112 "gcode.y"
	{ gcode_append(ctx, T_LN, 0); }
break;
case 42:
#line 113 "gcode.y"
	{ gcode_append(ctx, T_ROUND, 0); }
break;
case 43:
#line 114 "gcode.y"
	{ gcode_append(ctx, T_SQRT, 0); }
break;
case 44:
#line 115 "gcode.y"
	{ gcode_append(ctx, PUSH_NUM, yystack.l_mark[0].num); }
break;
case 45:
#line 116 "gcode.y"
	{ gcode_append(ctx, PUSH_NUM, yystack.l_mark[0].num); }
break;
#line 832 "gcode.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = YYLEX) < 0) yychar = YYEOF;
#if YYDEBUG
            if (yydebug)
            {
                yys = yyname[YYTRANSLATE(yychar)];
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == YYEOF) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (YYINT) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    YYERROR_CALL("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
