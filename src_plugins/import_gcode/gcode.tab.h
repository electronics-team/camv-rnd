#define T_NUM 257
#define T_DEC 258
#define T_CHR 259
#define T_NL 260
#define T_LINENO 261
#define T_ACOS 262
#define T_ASIN 263
#define T_ATAN 264
#define T_ABS 265
#define T_COS 266
#define T_SIN 267
#define T_TAN 268
#define T_FIX 269
#define T_FUP 270
#define T_EXP 271
#define T_LN 272
#define T_ROUND 273
#define T_SQRT 274
#define T_MOD 275
#define UMINUS 276
#define UPLUS 277
#define T_OR 278
#define T_XOR 279
#define T_AND 280
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union {
	double num;
	int chr;
} YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
extern YYSTYPE gcodelval;
