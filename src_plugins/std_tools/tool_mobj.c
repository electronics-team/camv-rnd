#include "crosshair.h"
#include "data.h"
#include <librnd/hid/hid.h>
#include <librnd/hid/hid_inlines.h>
#include <librnd/core/rnd_conf.h>

static void tool_mobj_init(void)
{
}

static void tool_mobj_uninit(void)
{
}

static void tool_mobj_press(rnd_design_t *hl)
{
	long lid;
	rnd_coord_t slop = rnd_gui->coord_per_pix * 3 / 2;
	camv_rtree_box_t sb;

	sb.x1 = camv.crosshair_x - slop;
	sb.y1 = camv.crosshair_y - slop;
	sb.x2 = camv.crosshair_x + slop;
	sb.y2 = camv.crosshair_y + slop;

	for(lid = camv.layers.used-1; lid >= 0; lid--) {
		camv_rtree_it_t it;
		camv_layer_t *ly = camv.layers.array[lid];
		camv_any_obj_t *obj = camv_rtree_first(&it, &ly->objs, &sb);
		if ((obj != NULL) && (obj->proto.calls->isc_box(obj, &sb))) {
			rnd_message(RND_MSG_WARNING, "%s ", ly->name, obj->proto.type);
			switch(obj->proto.type) {
					case CAMV_OBJ_ARC:
						rnd_message(RND_MSG_WARNING, "Arc: center %m+%$mD radius=%$mN\n thickness=%$mN\n",
							rnd_conf.editor.grid_unit->allow,
							obj->arc.cx, obj->arc.cy, obj->arc.r,
							obj->line.thick);
						break;
					case CAMV_OBJ_LINE:
						rnd_message(RND_MSG_WARNING, "Line: %m+%$mD to %$mD\n length=%$mN thickness=%$mN\n",
							rnd_conf.editor.grid_unit->allow,
							obj->line.x1, obj->line.y1, obj->line.x2, obj->line.y2,
							(rnd_coord_t)rnd_distance(obj->line.x1, obj->line.y1, obj->line.x2, obj->line.y2),
							obj->line.thick);
						break;
					case CAMV_OBJ_POLY:
						rnd_message(RND_MSG_WARNING, "Polygon\n");
						break;
					case CAMV_OBJ_GRP:
						rnd_message(RND_MSG_WARNING, "Group\n");
						break;
					case CAMV_OBJ_TEXT:
						rnd_message(RND_MSG_WARNING, "Text\n");
						break;
					case CAMV_OBJ_invalid:
					case CAMV_OBJ_max:
						rnd_message(RND_MSG_WARNING, "<invalid object type>\n");
						break;
			}
		}
	}
}

static void tool_mobj_release(rnd_design_t *hl)
{
}

static void tool_mobj_adjust_attached_objects(rnd_design_t *hl)
{
}


static void tool_mobj_draw_attached(rnd_design_t *hl)
{
}

rnd_bool tool_mobj_undo_act(rnd_design_t *hl)
{
	return 0;
}

rnd_bool tool_mobj_redo_act(rnd_design_t *hl)
{
	return 0;
}



/* XPM */
static const char *mobj_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 4 1",
"  c #000000",
". c #7A8584",
"X c #6EA5D7",
"O c None",
/* pixels */
"OOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOO..OOOOOOOOOO",
"OOOOOOOO....OOOOOOOOO",
"OOOOOOO..OO..OOOOOOOO",
"OOOOOOO..OO..OOOOOOOO",
"OOOOOOOX....XOOOOOOOO",
"OOOOOOOXO..OXOOOOOOOO",
"OOOOOOOXOOOOXOOOOOOOO",
"OOOOOOOXOOOOXOOOOOOOO",
"OOOOOOOXXXXXXOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOO",
"O OOO OO  OO   OOO  O",
"O  O  O OO O OO OOO O",
"O O O O OO O OO OOO O",
"O OOO O OO O    OOO O",
"O OOO O OO O OO OOO O",
"O OOO O OO O OO OOO O",
"O OOO O OO O OO O O O",
"O OOO OO  OO   OO  OO",
"OOOOOOOOOOOOOOOOOOOOO"
};


static rnd_tool_t camv_rnd_tool_mobj = {
	"mobj", NULL, std_tools_cookie, 100, mobj_icon, RND_TOOL_CURSOR_NAMED("pencil"), 0,
	tool_mobj_init,
	tool_mobj_uninit,
	tool_mobj_press,
	tool_mobj_release,
	tool_mobj_adjust_attached_objects,
	tool_mobj_draw_attached,
	tool_mobj_undo_act,
	tool_mobj_redo_act
};
