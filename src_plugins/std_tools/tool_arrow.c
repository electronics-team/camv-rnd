#include "crosshair.h"
#include "data.h"
#include <librnd/hid/hid.h>
#include <librnd/hid/hid_inlines.h>

static void tool_arrow_init(void)
{
}

static void tool_arrow_uninit(void)
{
}

static void tool_arrow_press(rnd_design_t *hl)
{
}

static void tool_arrow_release(rnd_design_t *hl)
{
}

static void tool_arrow_adjust_attached_objects(rnd_design_t *hl)
{
}


static void tool_arrow_draw_attached(rnd_design_t *hl)
{
}

rnd_bool tool_arrow_undo_act(rnd_design_t *hl)
{
	return 0;
}

rnd_bool tool_arrow_redo_act(rnd_design_t *hl)
{
	return 0;
}



/* XPM */
static const char *arrow_icon[] = {
/* columns rows colors chars-per-pixel */
"21 21 3 1",
"  c #000000",
". c #6EA5D7",
"o c None",
/* pixels */
"oo .. ooooooooooooooo",
"oo .... ooooooooooooo",
"ooo ...... oooooooooo",
"ooo ........ oooooooo",
"ooo ....... ooooooooo",
"oooo ..... oooooooooo",
"oooo ...... ooooooooo",
"ooooo .. ... oooooooo",
"ooooo . o ... ooooooo",
"oooooooooo ... oooooo",
"ooooooooooo .. oooooo",
"oooooooooooo  ooooooo",
"ooooooooooooooooooooo",
"ooo ooo    ooo    ooo",
"oo o oo ooo oo ooo oo",
"oo o oo ooo oo ooo oo",
"o ooo o    ooo    ooo",
"o ooo o ooo oo ooo oo",
"o     o ooo oo ooo oo",
"o ooo o ooo oo ooo oo",
"o ooo o ooo oo ooo oo"
};


static rnd_tool_t camv_rnd_tool_arrow = {
	"arrow", NULL, std_tools_cookie, 50, arrow_icon, RND_TOOL_CURSOR_NAMED("left_ptr"), 0,
	tool_arrow_init,
	tool_arrow_uninit,
	tool_arrow_press,
	tool_arrow_release,
	tool_arrow_adjust_attached_objects,
	tool_arrow_draw_attached,
	tool_arrow_undo_act,
	tool_arrow_redo_act
};
