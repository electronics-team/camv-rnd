/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef CAMV_OBJ_COMMON_H
#define CAMV_OBJ_COMMON_H

#include <librnd/core/global_typedefs.h>
#include "camv_typedefs.h"
#include "rtree.h"
#include <librnd/hid/hid.h>
#include <librnd/font/xform_mx.h>


typedef enum camv_objtype_e {
	CAMV_OBJ_invalid,
	CAMV_OBJ_ARC,
	CAMV_OBJ_LINE,
	CAMV_OBJ_POLY,
	CAMV_OBJ_GRP,
	CAMV_OBJ_TEXT,
	CAMV_OBJ_max
} camv_objtype_t;

typedef struct camv_objcalls_s {
	camv_any_obj_t *(*alloc)(void);
	void (*free_fields)(camv_any_obj_t *obj);
	void (*draw)(camv_any_obj_t *obj, rnd_hid_gc_t gc, rnd_xform_mx_t *mx);
	void (*bbox)(camv_any_obj_t *obj);
	void (*copy)(camv_any_obj_t *dst, const camv_any_obj_t *src);
	void (*move)(camv_any_obj_t *o, rnd_coord_t dx, rnd_coord_t dy); /* does not do any rtree administration, but bbox is updated if it was originally valid */
	int (*isc_box)(const camv_any_obj_t *o, const camv_rtree_box_t *box);

} camv_objcalls_t;

#define CAMV_ANY_PRIMITIVE_FIELDS \
	camv_rtree_box_t bbox; \
	camv_objtype_t type; \
	const camv_objcalls_t *calls; \
	camv_layer_t *parent_layer; \
	camv_any_obj_t *parent_obj; \
	unsigned bbox_valid:1


/* NOTE: ->parent_layer is NULL for grp member objects and ->parent_obj is NULL for normal objects */

#endif
