/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include <stdlib.h>

#include "event.h"
#include <librnd/core/rnd_conf.h>
#include <librnd/hid/grid.h>
#include <librnd/hid/hid.h>
#include <librnd/hid/hid_inlines.h>
#include <librnd/hid/tool.h>

#include "data.h"
#include "crosshair.h"
#include "conf_core.h"

rnd_hid_gc_t camv_crosshair_gc;

void camv_hidlib_crosshair_move_to(rnd_design_t *hl, rnd_coord_t abs_x, rnd_coord_t abs_y, int mouse_mot)
{
	/* grid fit */
	abs_x = rnd_grid_fit(abs_x, hl->grid, hl->grid_ox);
	abs_y = rnd_grid_fit(abs_y, hl->grid, hl->grid_ox);
	camv.crosshair_x = abs_x;
	camv.crosshair_y = abs_y;

	/* update the GUI */
	rnd_hid_notify_crosshair_change(hl, rnd_false);
	rnd_render->set_crosshair(rnd_render, abs_x, abs_y, 0);
	rnd_tool_adjust_attached(hl);
	rnd_hid_notify_crosshair_change(hl, rnd_true);
}

void camv_draw_attached(rnd_design_t *hidlib, rnd_bool inhibit_drawing_mode)
{
	rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_RESET, 1, NULL);
	rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_POSITIVE_XOR, 1, NULL);

	rnd_render->set_color(camv_crosshair_gc, &conf_core.appearance.color.attached);
	rnd_tool_draw_attached(hidlib);

	rnd_render->set_drawing_mode(rnd_render, RND_HID_COMP_FLUSH, 1, NULL);
}

void camv_crosshair_gui_init(void)
{
	camv_crosshair_gc = rnd_hid_make_gc();
	rnd_hid_set_draw_xor(camv_crosshair_gc, 1);
	camv.hidlib.grid = rnd_conf.editor.grid;
}

void camv_crosshair_gui_uninit(void)
{
	rnd_hid_destroy_gc(camv_crosshair_gc);
}

