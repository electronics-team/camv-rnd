/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include <librnd/config.h>

#include <librnd/core/vtc0.h>

#include "obj_poly.h"
#include "obj_any.h"

#include "geo.h"
#include <gengeo2d/box.h>

static void camv_poly_free_fields(camv_any_obj_t *obj)
{
	vtc0_uninit(&obj->poly.points);
	obj->poly.x = obj->poly.y = NULL;
	obj->poly.len = 0;
}

static void camv_poly_draw(camv_any_obj_t *obj, rnd_hid_gc_t gc, rnd_xform_mx_t *mx)
{
	if (mx != NULL) {
		static vtc0_t x, y;
		long n;
		if (obj->poly.len > x.alloced) {
			vtc0_enlarge(&x, obj->poly.len);
			vtc0_enlarge(&y, obj->poly.len);
		}
		for(n = 0; n < obj->poly.len; n++) {
			x.array[n] = rnd_xform_x((*mx), obj->poly.x[n], obj->poly.y[n]);
			y.array[n] = rnd_xform_y((*mx), obj->poly.x[n], obj->poly.y[n]);
		}
		rnd_render->fill_polygon(gc, obj->poly.len, x.array, y.array);
	}
	else
		rnd_render->fill_polygon(gc, obj->poly.len, obj->poly.x, obj->poly.y);
}

static void camv_poly_bbox(camv_any_obj_t *obj)
{
	rnd_cardinal_t n;
	const rnd_coord_t *x, *y;

	x = obj->poly.x;
	y = obj->poly.y;
	obj->poly.bbox.x1 = obj->poly.bbox.x2 = *x;
	obj->poly.bbox.y1 = obj->poly.bbox.y2 = *y;
	for(x++, y++, n = 1; n < obj->poly.len; n++,x++,y++) {
		if (*x < obj->poly.bbox.x1) obj->poly.bbox.x1 = *x;
		if (*x > obj->poly.bbox.x2) obj->poly.bbox.x2 = *x;
		if (*y < obj->poly.bbox.y1) obj->poly.bbox.y1 = *y;
		if (*y > obj->poly.bbox.y2) obj->poly.bbox.y2 = *y;
	}
	obj->poly.bbox_valid = 1;
}

static void camv_poly_copy(camv_any_obj_t *dst, const camv_any_obj_t *src)
{
	memcpy(&dst->proto, &src->proto, sizeof(src->proto));

	dst->poly.points.array = NULL;
	dst->poly.points.alloced = dst->poly.points.used = 0;
	vtc0_resize(&dst->poly.points, src->poly.points.used);
	memcpy(dst->poly.points.array, src->poly.points.array, src->poly.points.used * sizeof(rnd_coord_t));

	dst->poly.len = src->poly.len;
	dst->poly.x = dst->poly.points.array;
	dst->poly.y = dst->poly.points.array+dst->poly.len;
}

static void camv_poly_move(camv_any_obj_t *o, rnd_coord_t dx, rnd_coord_t dy)
{
	rnd_cardinal_t n;

	for(n = 0; n < o->poly.len; n++) {
		o->poly.x[n] += dx;
		o->poly.y[n] += dy;
	}
	if (o->poly.bbox_valid) {
		o->poly.bbox.x1 += dx; o->poly.bbox.y1 += dy;
		o->poly.bbox.x2 += dx; o->poly.bbox.y2 += dy;
	}
}

static camv_any_obj_t *camv_poly_alloc(void) { return (camv_any_obj_t *)camv_poly_new(); }

int camv_poly_isc_box(const camv_any_obj_t *o, const camv_rtree_box_t *box)
{
	rnd_cardinal_t n;
	g2d_box_t bx;

	bx.p1.x = box->x1; bx.p1.y = box->y1;
	bx.p2.x = box->x2; bx.p2.y = box->y2;

	TODO("geo: check if any box point is within the poly");

	/* check if any poly point is within the box */
	for(n = 0; n < o->poly.len; n++) {
		g2d_vect_t pt;
		pt.x = o->poly.x[n]; pt.y = o->poly.y[n];
		if (g2d_point_in_box(&bx, pt))
			return 1;
	}
	return 0;
}

static const camv_objcalls_t camv_poly_calls = {
	camv_poly_alloc,
	camv_poly_free_fields,
	camv_poly_draw,
	camv_poly_bbox,
	camv_poly_copy,
	camv_poly_move,
	camv_poly_isc_box
};

void camv_poly_init(camv_poly_t *poly)
{
	memset(poly, 0, sizeof(camv_poly_t));
	poly->type = CAMV_OBJ_POLY;
	poly->calls = &camv_poly_calls;
}

camv_poly_t *camv_poly_new(void)
{
	camv_poly_t *res = malloc(sizeof(camv_poly_t));
	camv_poly_init(res);
	return res;
}

void camv_poly_allocpts(camv_poly_t *poly, rnd_cardinal_t len)
{
	poly->len = len;
	vtc0_resize(&poly->points, len*2);
	poly->points.used = poly->points.alloced = len*2;
	poly->x = poly->points.array;
	poly->y = poly->points.array+len;
}
