#ifndef CAMV_CONF_CORE_H
#define CAMV_CONF_CORE_H

#include <librnd/core/conf.h>
#include <librnd/core/color.h>

typedef struct {
	const struct { /* appearance */
		struct { /* color */
			RND_CFT_COLOR layer[16];       /* default layer colors; when a new layer is created, a color from this list is assigned initially */
			RND_CFT_COLOR attached;
		} color;
		RND_CFT_STRING default_font;     /* OBSOLETE: used to be path to a ttf font; camv-rnd uses an embedded internal vector font now */
		RND_CFT_BOOLEAN bbox_debug;      /* enable rendering bounding box of objects, for debugging */
	} appearance;
	const struct rc { /* rc */
		RND_CFT_BOOLEAN error_on_bad_cli_files;
	} rc;
} conf_core_t;


void conf_core_init();
extern conf_core_t conf_core;

#endif
