#ifndef CAMV_TYPEDEFS_H
#define CAMV_TYPEDEFS_H

typedef struct camv_design_s camv_design_t;
typedef struct camv_layer_s camv_layer_t;
typedef union camv_any_obj_u camv_any_obj_t;
typedef struct camv_grp_s camv_grp_t;

#endif
