/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#ifndef CAMV_OBJ_TEXT_H
#define CAMV_OBJ_TEXT_H

#include "obj_common.h"
#include <librnd/hid/pixmap.h>

typedef struct camv_text_s {
	CAMV_ANY_PRIMITIVE_FIELDS;
	rnd_coord_t x, y;         /* placement: specify center bottom point of text */
	double rot;               /* in degrees */
	char *s;

	/* internal/cache */
	rnd_coord_t cx[4], cy[4]; /* 4 corners of the rotated bbox */
	rnd_coord_t dx, dy;       /* offsets from x,y to librnd font renderer */
} camv_text_t;

void camv_text_init(camv_text_t *text);
camv_text_t *camv_text_new(void);
void camv_text_update(rnd_design_t *hidlib, camv_text_t *text, camv_layer_t *color_ly); /* called to re-render the pixmap after field changes */

#endif
