/*
 *                            COPYRIGHT
 *
 *  camv-rnd - electronics-related CAM viewer
 *  Copyright (C) 2019,2024 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/camv-rnd
 *    lead developer: http://repo.hu/projects/camv-rnd/contact.html
 *    mailing list: camv-rnd (at) list.repo.hu (send "subscribe")
 */

#include "config.h"

#include <librnd/config.h>

#include <librnd/core/actions.h>
#include <librnd/core/compat_misc.h>
#include "event.h"

#include "data.h"
#include "plug_io.h"
#include "plug_io_act.h"

static const char *plug_io_cookie = "plug_io_act";

static const char camv_acts_LoadFrom[] = "LoadFrom(Layer|Project,filename[,format])";
static const char camv_acth_LoadFrom[] = "Load project or layer data from a file.";
fgw_error_t camv_act_LoadFrom(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	const char *op, *name, *format = NULL;

	RND_ACT_CONVARG(1, FGW_STR, LoadFrom, op = argv[1].val.str);
	RND_ACT_CONVARG(2, FGW_STR, LoadFrom, name = argv[2].val.str);
	RND_ACT_MAY_CONVARG(3, FGW_STR, LoadFrom, format = argv[3].val.str);

	if (rnd_strcasecmp(op, "layer") == 0) {
		if (camv_io_load(&camv, name) != 0) {
			rnd_message(RND_MSG_ERROR, "Can not load file '%s'\n", name);
			RND_ACT_IRES(-1);
			return 0;
		}
		rnd_event(&camv.hidlib, CAMV_EVENT_LAYERS_CHANGED, NULL);
	}
	else if (rnd_strcasecmp(op, "project") == 0) {
		TODO("the actual project load");
		rnd_message(RND_MSG_ERROR, "LoadFrom(project,...) not yet implemented\n");
	}
	else
		RND_ACT_FAIL(LoadFrom);

	RND_ACT_IRES(0);
	return 0;
}

static const char camv_acts_SaveTo[] = "SaveTo(design, filename[,format])";
static const char camv_acth_SaveTo[] = "Save design to a file.";
fgw_error_t camv_act_SaveTo(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	rnd_design_t *hl = RND_ACT_DESIGN;
	camv_design_t *camv = (camv_design_t *)hl;
	const char *op, *name, *format = "tedax";

	RND_ACT_CONVARG(1, FGW_STR, SaveTo, op = argv[1].val.str);
	RND_ACT_CONVARG(2, FGW_STR, SaveTo, name = argv[2].val.str);
	RND_ACT_MAY_CONVARG(3, FGW_STR, SaveTo, format = argv[3].val.str);

	if (rnd_strcasecmp(op, "design") == 0) {
		if (camv_save_design(camv, name, format) != 0) {
			rnd_message(RND_MSG_ERROR, "Can not save file '%s'\n", name);
			RND_ACT_IRES(-1);
			return 0;
		}
	}
	else {
		rnd_message(RND_MSG_ERROR, "Invalid first argument\n");
		RND_ACT_IRES(-1);
		return 0;
	}

	RND_ACT_IRES(0);
	return 0;
}

static const char camv_acts_GroupLoad[] = "GroupLoad(begin|end)";
static const char camv_acth_GroupLoad[] = "Starts or ends a group load of files.";
fgw_error_t camv_act_GroupLoad(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	rnd_design_t *hl = RND_ACT_DESIGN;
	camv_design_t *camv = (camv_design_t *)hl;
	const char *op;

	RND_ACT_CONVARG(1, FGW_STR, GroupLoad, op = argv[1].val.str);
	if (strcmp(op, "begin") == 0) camv_group_load_begin(camv);
	else if (strcmp(op, "end") == 0) camv_group_load_end(camv);
	else {
		rnd_message(RND_MSG_ERROR, "Invalid GroupLoad first argument\n");
		RND_ACT_IRES(-1);
		return 0;
	}

	RND_ACT_IRES(0);
	return 0;
}

static rnd_action_t camv_plug_io_act_list[] = {
	{"LoadFrom", camv_act_LoadFrom, camv_acth_LoadFrom, camv_acts_LoadFrom},
	{"SaveTo", camv_act_SaveTo, camv_acth_SaveTo, camv_acts_SaveTo},
	{"GroupLoad", camv_act_GroupLoad, camv_acth_GroupLoad, camv_acts_GroupLoad}
};

void camv_plug_io_act_uninit(void)
{
	rnd_remove_actions_by_cookie(plug_io_cookie);
}

void camv_plug_io_act_init(void)
{
	RND_REGISTER_ACTIONS(camv_plug_io_act_list, plug_io_cookie);
}
