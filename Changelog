camv-rnd 1.1.5 (r952)
~~~~~~~~~~~~~~~~~~~~~
	[build]
		-Update: bump required librnd version to >=4.2 (for the svg export color space feature)
		-Fix: make distclean removes everything generated
		-Fix: don't assume libs/ldl exist (win32 cross compilation)
		-Fix: remove hardwired -Dinline=, incompatible with the win32 cross compile
		-Fix: base config.h's selection between static and runtime prefix not on win32 but RND_WANT_FLOATING_FHS
		-Fix: /local/camv/want_static_librnd is not a mandatory scconfig variable
		-Add: implement /local/camv/want_static_librnd for win32 cross-compilation
		-Add: include socket ldlibs, relevant on win32

	[core]
		-Fix: don't redeclare rnd_xform_t, it's coming from librnd (still need to declre rnd_xform_s tho)
		-Fix: conf dir is runtime built in floating fhs mode
		-Fix: invalid memory read on Zoom() called with no args
		-Add: rename font to camv_font and make it public so that input formats that need custom font rendering can rely on the embedded font
		-Add: --help invocation prints available print and export plugins (Ringdove conventions)
		-Add: implement standard preview_draw
		-Add: InvertLayer() action and menu
		-Add: gui actions accept "last" as valid layer index
		-Add: expose ExecuteFile() action
		-Add: ColorLayer() action
		-Add: VisLayer action (visibility change with the same addressing as other core layer commands)
		-Add: GroupLoad() action so that order of layers loaded from a script is preserved
		-Add: startup action scripts with -s

	[dialogs]
		-Add: viewport dialog

	[doc]
		-Import: INSTALL.librnd.txt from pcb-rnd
		-Add: user doc index links appendix docs
		-Add: screenshot

	[export_svg]
		-Add: color space export option (color/grayscale/b&w)

	[gui]
		-Fix: need to update layer visibility indication in layersel on layer vis change ev

	[import_excellon]
		-Fix: return after fully parsing METRIC even if it had no arguments
		-Fix: do not choke on METRIC,00.00 lines

	[import_gcode]
		-Fix: more portable switch macros that don't rely on compile time double->integer conversions
		-Fix: don't abuse payload=-1 for indicating a pop() request, legit input can have -1 for X or Y easily; rather have a dedicated field for this in the instruction struct
		-Add: remember line number per instruction to ease debug
		-Add: local implementation of non-c89-portable round()

	[import_gerb]
		-Fix: C89 violation with excess semicolon
		-Fix: multi-quadrant arc with delta-angle 0 means full circle
		-Cleanup: GVT redefinition warnings

	[import_hpgl]
		-Add: new plugin frame for loading HP-GL files

	[io_tedax]
		-Cleanup: printf %p expects (void *), make the casts

camv-rnd 1.1.4 (r811)
~~~~~~~~~~~~~~~~~~~~~
	[build]
		-Change: depend on librnd >=4.1.0

	[core]
		-Fix: typo in file name in error message when failing to load from CLI
		-Del: local transformation matrix reverse functions in favor of librnd's

	[doc]
		-Fix: we no longer depend on freetype

	[gui]
		-Add: enable multiple selection in the file selection dialog for laoding multiple files at once
		-Add: librnd-standard Export() action

	[sign]
		-Add: new repo.hu crl for 2023

camv-rnd 1.1.3 (r785)
~~~~~~~~~~~~~~~~~~~~~
	[core]
		-Fix: set arc object stroke shape for bbox calculation to avoid depending on uninitialized stack variable
		-Fix: typo in file name in error message when failing to load from CLI
		-Cleanup: remove static inline in favor of RND_INLINE so -Dinline can be removed from the command line on pre-c99 hosts
		-Change: set y flip on design role, not cli role, to let actions override them more easily and make camv-rnd more consistent with pcb-rnd
		-Add: menu/hotkey for all combinations of tab (using SwapSides() to flip the board)

	[gui]
		-Fix: layersel respects sublayers when jumping over layers in layer moves
		-Fix: off-by-one bug when moving layer-with-sublayers up in the GUI
		-Fix: reset layer selector GUI struct for sublayers instead of just skipping them in the setup loop; moving sublayers around left stale gui structs behind, causing layer visibility icon mixup
		-Add: SwapSides() action, mirroring pcb-rnd's; second argument, optional 'S', swaps (reverses) layer order
		-Add: status line shows board flip state (whether display is from the top or bottom)

	[import_excellon]
		-Add: metric file formats without digits should assume 4:4 for now
		-Tune: change numeric format assumption for metric case to match the only sample we have

	[import_gerb]
		-Fix: ignore long commands OF and SF (not supported)
		-Fix: accept %AD* (empty aperture definition)

	[io_tedax]
		-Fix: reverse polarity of layers on save so the file is ordered in the natural top->bottom order

	[menu]
		-Add: {l o} for reverse layer ordering (in the view menu)


camv-rnd 1.1.2 (r746)
~~~~~~~~~~~~~~~~~~~~~
	[build]
		-Add: link librnd's font lib
		-Del: remove ./configure --fontdir (we have runtime font file path configuration instead)
		-Fix: do not detect librnd from ./configure as ./configure depends on librnd

	[core]
		-Fix: do not include 0;0 in layer bounding box, calculate the bbox only from objects
		-Fix: make up an 1*1mm bbox for empty layers so they are easier to handle on the GUI
		-Fix: initialize layer selection to -1 on start (no layer selected)
		-Move: index of selected layer from the gui plugin to camv_design_t because some non-gui actions will need it
		-Add: pass on and apply transformation matrix to object draw calls
		-Add: initialize layer xform matrix to ident when the layer is created
		-Add: draw code passes on layer xform matrix when enabled
		-Add: RotateLayer() action (sets xform mx and enables it)
		-Add: TranslateLayer()
		-Add: ScaleLayer()
		-Add: ResetLayer() action to reset the transformation matrix of a layer
		-Add: layer append API option for layer groups: reverse order within the group but make sure the group is appended on top
		-Add: group-load all layers specified on the command line so that order is preserved (from top to bottom)
		-Add: sublayer loading API so that multi-layer files can bypass the layer grouping logic and keep their sublayers ordered
		-Add: runtime configurable debug option for drawing object bounding boxes
		-Add: accurate bbox calculation for arc objects
		-Add: implement SaveTo() action (can save the whole design using the io plugin infra)

	[doc]
		-Add: datasheet
		-Add: mentions gmail is not supported
		-Add: link in route-rnd and sch-rnd as part of Ringdove
		-Add: install resources/ when installing doc
		-Update: action reference for the new layer transformation actions

	[export_ps]
		-Change: fill-page is default off for the ps export to get 1:1 prints

	[font]
		-Change: replace ttf with embedded vector font (pcb-rnd's)
		-Del: get rid of the freetype dependency and the ttf2bbox extern
		-Del: font size is not set per text object, remove the associated field
		-Add: center-align text: x;y coord should refer to bottom center point of the text object's bbox

	[gui]
		-Add: Save() and SaveAs() actions
		-Add: layersel debug option (compile time)

	[import_gerb]
		-Fix: create sublayers using the sublayer API to preserve layer ordering

	[io_tedax]
		-Fix: argument parsing: used the wrong number-of-arguments limit, leads to segfault if there are too many args
		-Fix: increase number of args accepted to the theoretical limit the format permits
		-Fix: preserve order of sublayers while loading a multi-polarity layer (using the sublayer append API)
		-Add: implement save prio call for standard io hooks

	[menu]
		-Add: save desgin that calls Save()

	[std_tools]
		-Add: measurement tool: rotate text to line angle for better alignment


camv-rnd 1.1.1 (r662)
~~~~~~~~~~~~~~~~~~~~~
	[build]
		-Fix: Makefile.depgen is generated and shouldn't be part of the repo
		-Fix: .builtin.pups shouldn't be part of the repo, it's generated
		-Change: switch over to librnd4 installation
		-Cleanup: missing #include (exposed by hid.h not always included)
		-Add: generate the usual set of installation path #defines in config.h

	[core]
		-Fix: uninit librnd on exit so that cli history is saved and plugins are properly unloaded
		-Cleanup: unregister plug_io_act.[ch] actions on exit
		-Add: cli history config in the default conf file
		-Add: emit RND_EVENT_LOAD_* events

	[dialogs]
		-Fix: plugin should be in the gui package
		-Fix: dialgos should be in camv-rnd-lib-gui package
		-Fix: use the right config node path for setting CLI history size in preferences general tab
		-Cleanup: rename pcb_dlg_pref_ to camv_dlg_pref_ to keep namespace clean

	[export_png]
		-Cleanup: png export invocation example in help: replace .pcb to .gbr, that's the typical use case
		-Fix: set expose context ->design before calling main expose
		-Fix: don't use global desig when it is available from argument
		-Del: don't define set_crosshair, use hid_nogui fallback

	[export_ps]
		-Fix: set expose context ->design before calling main expose

	[export_svg]
		-Fix: set expose context ->design before calling main expose
		-Fix: don't use global desig when it is available from argument

	[gui]
		-Fix: unregister actions on uninit
		-Fix: unregister layersel events on uninit

	[import_gcode]
		-Move: gcode-specific plugin settings into a local plugin conf

	[librnd4]
		-Update: follow librd4 API changes

	[util]
		-Del: deblist.sh from doc Makefile - not used by anything anymore
		-Del: local implementation of awk_on_formats - use librnd's
		-Add: import sign/ from librnd, for release verification


camv-rnd 1.1.0 (r558)
~~~~~~~~~~~~~~~~~~~~~
	[build]
		-Fix: map_plugins target in the scconfig generated makefile
		-Fix: internal menu's source name
		-Fix: wrong path to the plugin dir in map_plugins
		-Fix: CFLAGS for make dep so that librnd's src_3rd is found
		-Fix: respect ./configure --confdir in Makefile.conf
		-Fix: config.h uses configured prefix and config path
		-Fix: sharedir and libdir need to end in /camv-rnd
		-Fix: LIBDIR and SHAREDIR should include /camv-rnd in Makefile.conf
		-Fix: use -L and -I to librnd when installed to non-standard path

	[core]
		-Fix: --version should print the full version info all other Ringdove apps print, in the same format
		-Fix: use rnd_hid_print_all_gui_plugins() for printing GUI HID plugins
		-Fix: use rnd_render instead of rnd_gui for rendering so exports would work
		-Fix: remember when io plugins are sorted
		-Fix: make sure -lrnd-poly actually links in the poly lib (plugins depend on this)
		-Add: export helper infra: default file name calculator
		-Add: default export file name can be derived from the long name of the first layer
		-Add: main_expose sets rnd_render to target hid so that lpr plugin's target exporter (ps) would be invoked correctly

	[doc]
		-Add: comment on BSD make in INSTALL
		-Add: install manual page
		-Import: packager's doc from sch-rnd
		-Import: appendix generation from pcb-rnd for acton refs, dialogs and formats

	[export_lpr]
		-Add: standard lpr export for printing (imported from sch-rnd)

	[export_png]
		-Add: plugin based on sch-rnd's implementation, using librnd pixmap export

	[export_ps]
		-Add: standard ps/eps exporter (from sch-rnd)

	[export_svg]
		-Add: standard svg export (from sch-rnd)

	[gui]
		-Change: move the plugin in the lib-gui package (sch-rnd conventions)
		-Add: standard print dialog from librnd

	[import_excellon]
		-Cleanup: const correctness

	[import_gcode]
		-Fix: extra cast to unsigned on the case values ot make sure the compiler doesn't think it's not integral

	[import_gerb]
		-Fix: don't use fabs() on coords
		-Fix: don't mix code and declarations (c89!)

	[menu]
		-Add: ringdove-standard export and print menu
		-Fix: print/export refers to drawing, not sheet (copy&paste error from sch-rnd)

	[scconfig]
		-Import: from sch-rnd; switch over ./configure to use scconfig

	[util]
		-Import: awk_on_formats script from sch-rnd (for the packager's doc script)
		-Import: list dialogs devhelper from sch-rnd

camv-rnd 1.0.3 (r453)
~~~~~~~~~~~~~~~~~~~~~
	[build]
		-Fix: remove Makefile.dep dependencies on installed librnd headers - librnd is as external as libfreetype

	[core]
		-Fix: Quit() performs clean HID-quit instead of raw exit(0)
		-Del: message() based About() implementation
		-Add: define layer selected event
		-Add: right click context menu for layer properties

	[dialogs]
		-Add: new plugin for hosting camv-rnd dialog boxes
		-Add: about dialog
		-Add: application-specific tab in preferences
		-Add: layer (property) dialog: color set button and short name change

	[gui]
		-Add: Layer(getidx) returns index of the currently selected layer (useful for scripting and cross-action calls)
		-Add: layersel emits layer_selected event whenever layer selection changes
		-Add: Layer(getlen) returns the number of layers
		-Add: extend layer action: Layer(setcolor, #rrggbb)
		-Add: layer action: Layer(rename, newname) changes the short name of a layer
		-Add: layersel action: Layer(select, idx) will select the layer of the given index

	[import_excellon]
		-Fix: in 'T' line: skip over S and F that may precede C
		-Fix: both X and Y are optional in a move
		-Fix: allow preserving last X or Y coordinate for partial lines that have only X or only Y
		-Fix: don't treat the negative sign as part of the digits
		-Add: accept header ,LZ and set leading-zero bit
		-Add: a new, safer digit handler that can deal with both leading and trailing zeros

	[import_gerb]
		-Fix: accept aperture re-definition (the reference gerber render does too), but throw a warning
		-Fix: accept '*' terminated command sequence that doesn't have any actual drawing code but has state change 'set' commands (e.g. G01*)
		-Fix: convert the error to warning that we get for empty command sequence (the reference viewer doesn't even mention it!) and expalain the situation better
		-Add: number redundant error messages to ease debug

camv-rnd 1.0.2 (r401)
~~~~~~~~~~~~~~~~~~~~~
	[core]
		-Add: free font pixmap on text object removal

	[gui]
		-Add: layersel: all visible/all invisible buttons

	[import_gcode]
		-Cleanup: use TODO instead of (not portable) #warning

	[import_gerb]
		-Fix: implicit decl

	[menu]
		-Fix: {l r} wrong function arg: layer() has del, not remove
		-Add: preferences dialog, bound to {i c p}, in file menu

camv-rnd 1.0.1 (r375)
~~~~~~~~~~~~~~~~~~~~~
	[build]
		-Fix: central Makefile uses the FORCE - required on lame, case insensitive filesystems where INSTALL is mistaken for install when running make install
		-Fix: respect LIBARCHDIR in all Makefiles so that install works even if librnd used a non-default LIBRARCHDIR
		-Fix: link librnd poly before hidlib - the poly lib depends on hidlib's heap
		-Fix: make clean removes camv-rnd executable
		-Add: make distclean
		-Add: ./configure --help

	[conf]
		-Fix: set default layer alpha to 75%; with 0 nothing would show

	[core]
		-Fix: don't crash on empty layer name in layer-by-name lookup
		-Fix: draw: main layer draw sequence so that layers are rendered properly on all gtk2_gdk, lesstif, gtk2_gl and gtk4_gl
		-Tune: increase rtree stack size (temporary workaround)
		-Cleanup: remove rnd_tool_gui_init() - it is implemented in librnd since 3.0.0
		-Add: if gtk2 HID is not available, also try gtk4 as fallback

	[doc]
		-Fix: we do have a make install
		-Fix: camv-rnd is not beta anymore
		-Add: link edakrill from the -rnd box
		-Add: INSTALL: freetype2 dep
		-Change: bump required librnd version to 3.1.0

	[gui]
		-Fix: layersel: don't crash on non-existing layers
		-Fix: remove the autoload line from pup because setting it 0 doesn't have the same effect and it is attempted to load too early

	[import_excellon]
		-Fix: wrong pointer deref on checking for excellon end instruction
		-Add: parse FMAT, remember file format version and throw warnings on inappropriate codes for a given version
		-Add: ignore G90 (absolute coord mode), throw error on G91 (increment coord mode)
		-Add: accept T0 for unloading the current tool (no tool selected)

	[import_gerb]
		-Fix: "close-poly-on-move" shouldn't close non-existing polygon
		-Fix: "close-poly-on-move" resets the poly after the move even if it doesn't need to be closed to make sure the starting coords are recorded properly
		-Fix: pass down gerber ctx to aperture macro coord transformation as global unit will be needed for the conversion
		-Fix: need to remember global unit setting for macro apertures
		-Fix: expression parser: wrong order of operands generated for unary minus
		-Fix: expression parser: typo caused syntax error or token-end not detected on '+' in numbers
		-Fix: support for optional arguments in macro aperture drawing primitives
		-Fix: step-rep: don't quit "looping" before the first iteration if the loop runs only once
		-Fix: do not crash on missing arc aperture, just throw an error
		-Fix: do not fetch the aperture while drawing poly contour
		-Fix: generate 0 sized oblong as 1 nm thin
		-Fix: make ignored case explicit (macro def shouldn't appear in render)
		-Import: gerbv issue 57 test case for aperture macro out-of-bounds indexing
		-Import: gerbv issue 56 test case for potential crash on invalid input
		-Import: gerbv issue 30 test case to trigger a potential bug
		-Add: parse fancy long command comments %TA, %TD, %TO, %TF (ignore them)

	[io_tedax]
		-Fix: missing switch case on text objects (there is no persistent text, shouldn't be saved)


camv-rnd 1.0.0 (r299)
~~~~~~~~~~~~~~~~~~~~~
Initial release.


